## c1sxxx-user 12 SP1A.210812.016 N981BXXU3FVC5 release-keys
- Manufacturer: samsung
- Platform: universal990
- Codename: c1s
- Brand: samsung
- Flavor: c1sxxx-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: N981BXXU3FVC5
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/c1sxxx/c1s:11/RP1A.200720.012/N981BXXU3FVC5:user/release-keys
- OTA version: 
- Branch: c1sxxx-user-12-SP1A.210812.016-N981BXXU3FVC5-release-keys
- Repo: samsung/c1s
